# kubernetes-imt
Conteneurisation d'une application de vote pour l'UE DevOps

## How to run it
- Suivre ce [tutoriel](https://helene-coullon.fr/pages/ue-nuage-login-23-24/tuto-gke/) pour les installations
- Suivre ce [tutoriel](https://gitlab.imt-atlantique.fr/login-nuage/project) partie **Kubernetes** pour mettre les images sur GCP. Prenez les images du [projet docker](https://gitlab.imt-atlantique.fr/c22dioll/docker-imt)
- Créez votre cluster ```gcloud container clusters create <name> --zone=<zone>```
- Déployez l'orchestration ```kubectl create -f deploy-app.yaml```